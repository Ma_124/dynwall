#!/usr/bin/env python3
import click
from configobj import ConfigObj
import os.path
import re


class RainmeterConfig(ConfigObj):
    pass
    # Workaround https://github.com/DiffSK/configobj/issues/181
    # _valueexp = re.compile(r'''^
    #     (?:
    #         (?:
    #             (
    #                 (?:
    #                     (?:
    #                         (?:".*?")|              # double quotes
    #                         (?:'.*?')|              # single quotes
    #                         (?:[^'",][^,]*?)        # unquoted
    #                     )
    #                     \s*,\s*                     # comma
    #                 )*      # match all list items ending in a comma (if any)
    #             )
    #             (
    #                 (?:".*?")|                      # double quotes
    #                 (?:'.*?')|                      # single quotes
    #                 (?:[^'",\s][^,]*?)|             # unquoted
    #                 (?:(?<!,))                      # Empty value
    #             )?          # last item in a list - or string value
    #         )|
    #         (,)             # alternatively a single comma - empty list
    #     )
    #     $''', re.VERBOSE)


def nor_rainmeter_metadata(sec):
    return sec != "Rainmeter" and sec != "Metadata"


@click.command()
@click.argument("ini")
@click.argument("skins")
@click.argument("yaml")
def main(**kwargs):
    ini = RainmeterConfig(kwargs["ini"])
    honeycomb_path = os.path.join(kwargs["skins"], "Honeycomb")

    with open(kwargs["yaml"], "w") as f:
        f.write("icons:\n")
        for sn in ini.keys():
            if not str(sn).startswith("Honeycomb\\"):
                continue
            sv = ini[sn]
            if str(sv["Active"]) != "1":
                continue
            try:
                skinini_path = os.path.join(honeycomb_path, str(sn)[10:], str(sn)[10:] + ".ini")
                skinini = RainmeterConfig(skinini_path)
                skinini_sec = skinini[filter(nor_rainmeter_metadata, skinini.keys()).__next__()]
                f.write("#  name: " + sn[10:] + "\n")
                f.write("- x: " + str(sv["WindowX"]) + "\n")
                f.write("  y: " + str(sv["WindowY"]) + "\n")
                f.write("  w: 90\n")
                f.write("  h: 90\n")
                f.write("  img: " + os.path.join(honeycomb_path, "@Resources", "Images",
                                                  str(skinini_sec["ImageName"])[10:]) + "\n")
                f.write("  cmd: \n")
            except Exception as err:
                print("Could not load: " + skinini_path + ":", err)
        f.write("# vim: et:ts=2:sw=2")


if __name__ == '__main__':
    main()
