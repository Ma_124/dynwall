#!/bin/bash
set -e

./gen.py $1
./build.sh
sh -c "kquitapp5 plasmashell && kstart5 plasmashell" &> /dev/null
