## Installing from source

```bash
$EDITOR config.yml
./build-refresh.sh config.yml
```

![Desktop > Right Click > Configure Desktop](https://ma124.js.org/img/kde_configure_desktop.png)
![Layout: Desktop; Wallpaper Type: DynWall](https://ma124.js.org/img/kde_desktop_settings_dynwall.png)
