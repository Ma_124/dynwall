//
// Created by ma_124 on 23.06.19.
//

#ifndef DYN_WALL_DYNAMICWALLPAPER_H
#define DYN_WALL_DYNAMICWALLPAPER_H

#include <QObject>
#include <QString>
#include <QProcess>
#include <iostream>
#include <stdlib.h>

class DynamicWallpaper : public QObject {
    Q_OBJECT
    Q_PROPERTY(QString launch WRITE setLaunch)
    Q_PROPERTY(QString qtVersion READ qtVersion)

public:
    explicit DynamicWallpaper(QObject* parent = nullptr);
    ~DynamicWallpaper() override;

    void setLaunch(const QString &launch);
    QString qtVersion();

Q_SIGNALS:

public Q_SLOTS:

private:
    Q_DISABLE_COPY(DynamicWallpaper)
};


#endif //DYN_WALL_DYNAMICWALLPAPER_H
