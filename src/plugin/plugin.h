//
// Created by ma_124 on 23.06.19.
//

#ifndef DYN_WALL_PLUGIN_H
#define DYN_WALL_PLUGIN_H

#include <QQmlExtensionPlugin>

class Plugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.qt-project.Qt.QQmlExtensionInterface")

public:
    void registerTypes(const char* uri) override;
};

#endif //DYN_WALL_PLUGIN_H
