#include "plugin.h"
#include "DynamicWallpaper.h"

#include <QQmlEngine>

void Plugin::registerTypes(const char* uri)
{
    Q_ASSERT(uri == QLatin1String("com.gitlab.ma124.internal.dynwall"));
    qmlRegisterType<DynamicWallpaper>(uri, 1, 0, "DynamicWallpaper");
}
