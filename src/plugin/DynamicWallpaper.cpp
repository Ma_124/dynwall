//
// Created by ma_124 on 23.06.19.
//

#include "DynamicWallpaper.h"

DynamicWallpaper::DynamicWallpaper(QObject* parent)
        : QObject(parent)
{
}

DynamicWallpaper::~DynamicWallpaper()
{
}

void DynamicWallpaper::setLaunch(const QString &launch) {
    QProcess proc;
    proc.setStandardOutputFile(QProcess::nullDevice());
    proc.setStandardErrorFile(QProcess::nullDevice());
    proc.startDetached(launch);
}

QString DynamicWallpaper::qtVersion() {
    return QString(qVersion());
}