#!/usr/bin/env python3
import yaml
import click


@click.command()
@click.argument("inp")
@click.argument("out")
def main(**kwargs):

    out = dict()
    with open(kwargs["inp"]) as f:
        inp = yaml.safe_load(f)
    out["wallpaper"] = inp["wallpaper"]
    ics = []
    xo = inp["xo"]
    yo = inp["yo"]
    xs = inp["xs"]
    ys = inp["ys"]
    for icon in inp["icons"]:
        x = xo + (xs * int(icon["x"]))
        y = yo + (ys * int(icon["y"]))
        if y % 2 != 0:
            x += xs/2

        ics.append({
            "x": int(inp["xo"]) + x,
            "y": int(inp["yo"]) + y,
            "w": 90,
            "h": 90,
            "img": icon["img"],
            "cmd": icon["cmd"],
        })
    out["icons"] = ics

    with open(kwargs["out"], "w") as f:
        yaml.safe_dump(out, f)


if __name__ == '__main__':
    main()
