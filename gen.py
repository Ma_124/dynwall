#!/usr/bin/python3
import yaml
import click
import os.path


def abspath(path):
    return os.path.abspath(os.path.expanduser(os.path.expandvars(path)))


@click.command()
@click.argument('src')
def main(**kwargs):
    if not os.path.isfile("src/package/metadata.desktop"):
        print("Call this script from the package root (where this file and the README.md are)")

    with open(kwargs["src"], "r") as cfgf:
        try:
            cfg = yaml.safe_load(cfgf)
            with open("src/package/contents/ui/main.qml", "w") as f:
                f.write('''
import QtQuick 2.9
import org.kde.plasma.core 2.0 as PlasmaCore

import QtQuick.Layouts 1.1
import QtQuick.Window 2.2

import org.kde.plasma.components 2.0 as PlasmaComponents
import org.kde.plasma.extras 2.0 as PlasmaExtras

import com.gitlab.ma124.internal.dynwall 1.0

// Direct changes in this file will be deleted by the gen.py tool

Image {
    id: root
    source: "''' + abspath(str(cfg['wallpaper'])) + '''"

    ListModel {
        id: modelIcons
''')
                for icon in cfg["icons"]:
                    f.write('''
        ListElement {
            X: ''' + str(icon['x']) + '''
            Y: ''' + str(icon['y']) + '''
            Width: ''' + str(icon['w']) + '''
            Height: ''' + str(icon['h']) + '''
            Source: "''' + os.path.abspath(str(icon['img'])) + '''"
            Exe: "''' + str(icon['cmd']) + '''"
        }
''')
                f.write('''
    }

    Repeater {
        model: modelIcons

        Image {
            x: X
            y: Y
            width: Width
            height: Height
            source: Source
            fillMode: Image.PreserveAspectCrop
            smooth: true
            mipmap: true
            opacity: 0.8

            MouseArea {
                anchors.fill: parent
                cursorShape: Qt.PointingHandCursor
                hoverEnabled: true
                onClicked: {
                    dynwall.launch = Exe
                }
                onEntered: {
                    parent.opacity = 1
                }
                onExited: {
                    parent.opacity = 0.8
                }
            }
        }
    }

    DynamicWallpaper {
        id: dynwall
    }
}
''')
        except yaml.YAMLError as exc:
            print(exc)


if __name__ == '__main__':
    main()
